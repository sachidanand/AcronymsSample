//
//  DetailViewController.swift
//  SampleCode
//
//  Created by  on 06/06/16.
//  Copyright © 2016 Exilant Technologies. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var longformLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var header: UILabel!
    
    @IBOutlet var variationsCount: UILabel!
    var detailItem: Acronym?
    var noOfVariations : Int? {
        return detailItem?.vars?.count
    }
    
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            self.longformLabel.text = detail.longForm
            self.frequencyLabel.text = String(detail.frequency)
            self.yearLabel.text = String(detail.yearOfAddition)
            self.variationsCount.text = String(noOfVariations!)
            self.header.text = detail.shortForm
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

