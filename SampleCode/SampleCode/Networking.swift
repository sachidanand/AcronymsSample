//
//  Networking.swift
//  SampleCode
//
//  Created by  on 20/06/16.
//  Copyright © 2016 Exilant Technologies. All rights reserved.
//

import Foundation

/*
    TransactionManager is a Singleton class designed to manage all the transactions. All the communication to server should go through the APIs of this class.

*/

class TransactionManager {
    
    static let sharedInstance = TransactionManager() // Class variable to refer to singleton instance.
    
    // Overridden to prevent users from creating a different instance.
    fileprivate init() {
        
    }
    
    fileprivate let baseURLString = "http://www.nactem.ac.uk/software/acromine/dictionary.py" // Base URL string.
    
    /*
        Instance method to get the acronym details. Takes acronym to search and a completion handler as parameters.
        Completion handler is executed on completion of the transaction.
    */
    func fetchDetailsForAcronym(_ searchString:String, withCompletionHandler:@escaping (_ data:Data?, _ error:Error?) -> Void) {
        let urlString = self.frameCompleteURL(withString: searchString) // Frame complete URL string using search criteria.
        if let url = URL(string: urlString) {
            let urlRequest = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data:Data?, response:URLResponse?, error:Error?) -> Void in
                var statusCode = 0;
                if let httpResponse = response as? HTTPURLResponse {
                    statusCode = httpResponse.statusCode
                }
                // Check status code and parse the fetched JSON.
                if statusCode == 200, let jsonData = data {
                    withCompletionHandler(jsonData, nil)
                }
                else if let transactionError = error {
                    print("Error with transaction: \(transactionError.localizedDescription)")
                    withCompletionHandler(nil, transactionError)
                }

            } )
            
            task.resume()
        }
        else {
            print("Error while creating URL!")
        }
        
    }
    
    
    fileprivate func frameCompleteURL(withString inString:String) -> String {
        return self.baseURLString + "?" + "sf=\(inString)"
    }
}

