//
//  MasterViewController.swift
//  SampleCode
//
//  Created by  on 06/06/16.
//  Copyright © 2016 Exilant Technologies. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UISearchBarDelegate {

    var detailViewController: DetailViewController? = nil
    var acronyms = [Acronym]()
    @IBOutlet weak var searchBar: UISearchBar!
    weak var activityIndicatorView: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
            self.searchBar.becomeFirstResponder()
            // Activity indicator view to show users about state of the transaction.
            let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
            activityIndicatorView.color = UIColor.black
            activityIndicatorView.center = self.tableView.center
            tableView.backgroundView = activityIndicatorView
             self.activityIndicatorView = activityIndicatorView
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            print("Showing detail screen")
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = acronyms[(indexPath as NSIndexPath).row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return acronyms.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let object = acronyms[(indexPath as NSIndexPath).row]
        cell.textLabel!.text = object.longForm // Set long form as table cell's value
        return cell
    }
    
    
    // Setting background color explicitly to support iPad as well.
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
    }

    // MARK: - UISearchBarDelegate Methods
    // Clear table view when the search criteria changes.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        acronyms.removeAll()
        self.tableView.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        print("Search button clicked")
        // format the search string eliminating white spaces, if any
        if let searchString = searchBar.text?.trimmingCharacters(in: CharacterSet.whitespaces) , searchString.characters.count > 0
        {
            searchForAcronym(searchString)
        }
        else {
            self.alertUserWith("Invalid text", message: "Please enter a valid acronym")
        }
    }
    
    
    // Search function
    func searchForAcronym(_ acronym:String) {
        print("Search begin")
        self.activityIndicatorView.startAnimating() // Start animating activity indicator

        TransactionManager.sharedInstance.fetchDetailsForAcronym(acronym) { (data, error) -> Void in
            DispatchQueue.main.async(execute: {
                self.activityIndicatorView.stopAnimating()
            })
            if let responseData = data {
                self.createAcronymsWithJSONData(responseData)
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            }
            else if let transactionError = error {
                print("Error with transaction: \(transactionError.localizedDescription)")
                self.alertUserWith("Error", message: transactionError.localizedDescription)
            }
        }
        
    }
    
    
    
    // MARK: - JSON Parsing
    // Parsing the JSON from the response and creating the model objects
    func createAcronymsWithJSONData(_ jsonData:Data) {
        
        do {
            let jsonArray = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! NSArray
            // Check if the response is empty and return if empty.
            guard jsonArray.count > 0 else {
                print("No Acronym Found!")
                self.alertUserWith("No data Found!", message: "Please try with a valid acronym")
                return
            }
            print("Parsing the JSON response.")
            if let acronymsDict = jsonArray.firstObject as? [String:AnyObject], let acronymsArray = acronymsDict["lfs"] as? NSArray {
                print("Creating the Acronym objects")
                for acronym1 in acronymsArray {
                    let acronym = acronym1 as! [String:AnyObject]
                    if let sf = acronymsDict["sf"] as? String,
                        let lf = acronym["lf"] as? String,
                        let freq = acronym["freq"] as? Int,
                        let since = acronym["since"] as? Int,
                        let variations = acronym["vars"] as? [[String:AnyObject]]
                    {
                        // Create the model object using the initializer.
                        let newAcronym = Acronym(sf:sf, lf: lf.capitalized, freq: freq, since: since, variations: variations)
                        self.acronyms.append(newAcronym)
                    }
                        
                    else {
                        print("JSON is not in the expected format")
                    }
                }
            }
            else {
                
                print("JSON is not in the expected format")
                
            }
        }
        catch let error {
            print("Error with Json parsing: \(error)")
            self.alertUserWith("Error", message: "Error with JSON parsing")
        }
    }

    
    // Function to show alert to users.
    func alertUserWith(_ title:String, message:String) {
        print("Alert:\(message)")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.searchBar.text = ""
                self.searchBar.becomeFirstResponder()
            })
        }))
        DispatchQueue.main.async { 
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
}

