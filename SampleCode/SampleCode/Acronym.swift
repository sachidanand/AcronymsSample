//
//  Acronym.swift
//  SampleCoding
//
//  Created by Sachidanand on 04/06/16.
//  Copyright © 2016 Sachidanand Hiremath. All rights reserved.
//

/*
    This class represents the Acronym objects in the project. With this class the model objects are created using the initializer method.
*/

import Foundation
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class Acronym {
    
    var shortForm : String
    var longForm : String
    var frequency : Int
    var yearOfAddition : Int
    var vars : Array<Acronym>? = []
    
    init(sf:String, lf:String, freq:Int, since:Int, variations:[[String:AnyObject]]?) {
        
        shortForm = sf
        longForm = lf
        frequency = freq
        yearOfAddition = since
        
        if variations?.count > 0 {
            for newDict in variations! {
                
                if let longForm = newDict["lf"] as? String,
                    let frequency = newDict["freq"] as? Int,
                    let since = newDict["since"] as? Int {
                        let newVariation = Acronym(sf:shortForm, lf: longForm, freq:frequency, since:since, variations: nil)
                        vars?.append(newVariation)
                }
                else {
                    print("Error while creating model objects. Please check the types of JSON elements.")
                }
            }
        }
    }
}
